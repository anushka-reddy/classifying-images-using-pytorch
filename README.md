# Classifying images using PyTorch

## Process

Initialize the Azure ML Workspace Connection

Data Management

2.1. Retrieve and decompress the dataset

2.2. Transfer the dataset to the Azure ML datastore

2.3. Organize the images into a structured dataset

2.4. Visual inspection of the dataset samples

Model Development

3.1. Draft the model training code

3.2. Configure and execute a PyTorch ScriptRunConfig session

3.3. Catalog the trained model within Azure ML

3.4. Retrieve the model and perform local tests

Model Deployment

4.1. Script the model inference logic

4.2. Assemble the runtime environment specifications

4.3. Construct the inference configuration

4.4. Launch the model on Azure Container Instance

4.5. Deploy the model on Azure Kubernetes Service

4.6. Validate the model through API endpoint testing

## Results of the model

![First Result](Result%20images/Screen_Shot_2024-03-04_at_12.12.51.png)
![Second Result](Result%20images/Screen_Shot_2024-03-04_at_12.13.03.png)


## Thank you Microsoft Bootcamp facilitators for the support throughout this project.
